#!/bin/bash

default_port_number=1028
port_number_regex="([1-9]|[1-9][0-9]{1,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$"
invalid_number_of_arguments="Invalid number of arguments"

if [[ $# -eq 0 ]]; then
  echo "Missing command"
  echo "Usage: vsh [list|extract|browse|start|stop]"
  exit -1
fi

# Verify correct command and set "command" variable
case "$1" in
  -l|-list|--list|list|ls)
    command="client list" 
    source vsh_client
#    source vsh_list
    shift 1
    ;;
  -e|-extract|--extract|extract)
    command="client extract" 
    source vsh_client
    shift 1
    ;;
  -b|-browse|--browse|browse)
    command="client browse" 
    source vsh_client
    shift 1
    ;;
  -s|-start|--start|start)
    command="start_server" 
    source vsh_server
    shift 1
    ;;
  -stop|--stop|stop)
    command="stop_server" 
    source vsh_server
    shift 1
    ;;
  *)
    echo "Invalid command"
    echo "Usage: vsh [list|extract|browse|start|stop]"
    exit -1
esac

# Set args variable to contain all remainging arguments
args=$@

# Execute previously set command with previously set arguments
$command $args
